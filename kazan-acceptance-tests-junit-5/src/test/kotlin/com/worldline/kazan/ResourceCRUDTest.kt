package com.worldline.kazan

import com.worldline.kazan.clients.CredentialsBuilder
import com.worldline.kazan.clients.JacksonClientBuilder
import com.worldline.kazan.clients.ResourceManagementService
import com.worldline.kazan.clients.readBody
import com.worldline.kazan.dtos.ResourceDTO
import com.worldline.kazan.dtos.SynchronizationState
import org.assertj.core.api.Assertions.assertThat
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestName
import java.util.*


class `Resource CRUD Test` {

    // TODO replace hard coded URL with a property
    // TODO find a way to cleanup the test even if there assert errors

    @Rule @JvmField val name = TestName()

    val base64encoder = Base64.getEncoder()!!

    val url = "http://localhost:12000" // local
//    val url = "http://10.34.251.50:8001" // sandbox


    val client = JacksonClientBuilder.createClient<ResourceManagementService>(url) {
        it.header("TagLog", base64encoder.encodeToString(name.methodName.toByteArray()))
    }

    fun resourceManagement(init: CredentialsBuilder.() -> Unit) {
        com.worldline.kazan.clients.resourceManagement(client, init)
    }

    @Test
    fun `test resource creation`() {
        val projectReference = projectReferenceProvider()
        resourceManagement {
            val a149812 = logUser("a149812")
            a149812 {
                project(projectReference) {
                    name = "My Project"
                    description = "Simple POC"
                    put {
                        assertThat(status()).isCreated()
                    }

                    try {
                        val gitReference = gitReferenceProvider()
                        git(gitReference) {
                            name = "Mon git"
                            put {
                                assertThat(status()).isCreated()
                            }

                            // I should be able to get it immediatly even if the status is still 'issac'
                            get {
                                val git: ResourceDTO = readBody()
                                println(git)
                                with(git) {
                                    assertThat(reference).isEqualTo(gitReference)
                                    assertThat(name).isEqualTo("Mon git")
                                    assertThat(syncState).isEqualTo(SynchronizationState.ISAAC)
                                }
                            }

                            // creation should not take more than 10s
                            Thread.sleep(10000)
                            get {
                                val git: ResourceDTO = readBody()
                                with(git) {
                                    assertThat(reference).isEqualTo(gitReference)
                                    assertThat(name).isEqualTo("Mon git")
                                    assertThat(syncState).isEqualTo(SynchronizationState.ALIVE)
                                }
                            }
                        }


                    } finally {
                        delete {
                            println(status())
                        }
                    }
                }
            }
        }
    }

}