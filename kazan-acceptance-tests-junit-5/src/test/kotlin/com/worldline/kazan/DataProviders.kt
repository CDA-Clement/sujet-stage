package com.worldline.kazan

import java.util.concurrent.atomic.AtomicInteger

fun acceptanceTestReference(postfix: String) = "acceptance-test-$postfix"

fun acceptanceTestReference(type: String, increment: Int) =
        acceptanceTestReference("$type-${increment.toString().padStart(3, '0')}")

/**
 * Base class for String based reference.
 *
 * Each reference ends with an ID which is automatically incremented.
 */
abstract class StringBasedReferenceProvider(val type: String) : () -> String {
    private val id = AtomicInteger(0)
    override fun invoke(): String = acceptanceTestReference(type, id.addAndGet(1))
}

/**
 * Instance of #StringBasedReferenceProvider which provide reference for projects
 */
object projectReferenceProvider : StringBasedReferenceProvider("project")

/**
 * Instance of #StringBasedReferenceProvider which provide reference for resources
 */
object resourceReferenceProvider : StringBasedReferenceProvider("resource")

object gitReferenceProvider : StringBasedReferenceProvider("git")
object svnReferenceProvider : StringBasedReferenceProvider("svn")
object hgReferenceProvider : StringBasedReferenceProvider("hg")
object gitlabReferenceProvider : StringBasedReferenceProvider("gitlab")
object jenkinsReferenceProvider : StringBasedReferenceProvider("jenkins")

