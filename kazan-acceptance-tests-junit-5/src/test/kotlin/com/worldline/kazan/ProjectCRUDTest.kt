package com.worldline.kazan

import com.worldline.kazan.clients.CredentialsBuilder
import com.worldline.kazan.clients.JacksonClientBuilder
import com.worldline.kazan.clients.ResourceManagementService
import com.worldline.kazan.clients.readBody
import com.worldline.kazan.dtos.ProjectDTO
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assume.assumeThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInfo
import java.util.*

@DisplayName("Project CRUD Test")
class ProjectCRUDTest {

    // TODO replace hard coded URL with a property (#aurelie_au_secours)

    // TODO find a way to cleanup the test even if there assert errors
    // do NOT use "assert" in finally block because if we get an error, the "root" error will be replaced by this
    // new error
    // for now, the test display the status to show if the deletion is a success
    // can be replaced by a better warning message in case of error (like "Unable to delete the
    // test resource")


    val base64encoder = Base64.getEncoder()!!

        var currentMethodName = "unknown"

    val client = JacksonClientBuilder.createClient<ResourceManagementService>("http://localhost:12000") {
        it.header("TagLog", base64encoder.encodeToString(currentMethodName.toByteArray()))
    }

    fun resourceManagement(init: CredentialsBuilder.() -> Unit) {
        com.worldline.kazan.clients.resourceManagement(client, init)
    }

    @BeforeEach
    fun setCurrentTestMethodName(testInfo: TestInfo) {
        currentMethodName = testInfo.displayName
    }

    @Test
    @DisplayName("test project creation")
    fun testProjectCreation() {
        val projectReference = projectReferenceProvider()
        resourceManagement {
            val a149812 = logUser("a149812")
            a149812 {
                project(projectReference) {
                    get {
                        assumeThat(status(), `is`(404))
                    }

                    name = "My Project"
                    description = "Simple POC"
                    put {
                        assertThat(status()).isCreated()
                    }

                    try {

                        get {
                            assertThat(status()).isOK()
                            val project: ProjectDTO = readBody()
                            with(project) {
                                assertThat(reference).isEqualTo(projectReference)
                                assertThat(name).isEqualTo("My Project")
                                assertThat(description).isEqualTo("Simple POC")
                            }
                        }

                    } finally {
                        delete {
                            println(status())
                        }
                    }
                }
            }
        }
    }

    @Test
    @DisplayName("test create existing Project")
    fun testCreateExistingProject() {
        val projectReference = projectReferenceProvider()
        resourceManagement {
            val a149812 = logUser("a149812")
            val a149813 = logUser("a149813")
            a149812 {
                project(projectReference) {
                    name = "My Project"
                    description = "Simple POC"
                    put {
                        assertThat(status()).isCreated()
                    }
                }
            }

            try {

                a149813 {
                    project(projectReference) {
                        name = "Cool project"
                        description = ""
                        put {
                            assertThat(status()).isForbidden()
                        }
                    }
                }

                // project should not have been updated
                a149812 {
                    project(projectReference) {
                        get {
                            val project: ProjectDTO = readBody()
                            with(project) {
                                assertThat(reference).isEqualTo(projectReference)
                                assertThat(name).isEqualTo("My Project")
                                assertThat(description).isEqualTo("Simple POC")
                            }
                        }
                    }
                }
            } finally {
                // cleanup
                a149812 {
                    project(projectReference) {
                        delete {
                            println(status())
                        }
                    }
                }
            }
        }
    }

}