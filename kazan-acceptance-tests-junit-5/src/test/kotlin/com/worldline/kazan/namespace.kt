package com.worldline.kazan

import org.assertj.core.api.AbstractIntegerAssert
import javax.ws.rs.core.Response

// Add extensions to assert objects
fun AbstractIntegerAssert<*>.is2xx(): AbstractIntegerAssert<*>? = isBetween(200, 299)
fun AbstractIntegerAssert<*>.is4xx(): AbstractIntegerAssert<*>? = isBetween(400, 499)

fun AbstractIntegerAssert<*>.isOK(): AbstractIntegerAssert<*>? = isEqualTo(Response.Status.OK.statusCode)
fun AbstractIntegerAssert<*>.isCreated(): AbstractIntegerAssert<*>? = isEqualTo(Response.Status.CREATED.statusCode)
fun AbstractIntegerAssert<*>.isNoContent(): AbstractIntegerAssert<*>? = isEqualTo(Response.Status.NO_CONTENT.statusCode)
fun AbstractIntegerAssert<*>.isForbidden(): AbstractIntegerAssert<*>? = isEqualTo(Response.Status.FORBIDDEN.statusCode)
fun AbstractIntegerAssert<*>.isNotFound(): AbstractIntegerAssert<*>? = isEqualTo(Response.Status.NOT_FOUND.statusCode)

