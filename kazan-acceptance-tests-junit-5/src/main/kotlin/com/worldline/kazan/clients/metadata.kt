package com.worldline.kazan.clients

interface Metadata {
    fun toMetadata(): String? = gson.toJson(this)
}

data class JobTypeInformation(val jobtype: String?,
                              val description: String?,
                              val jobnametocopy: String?,
                              val scmkind: String?,
                              val url: String?,
                              val modulename: String?,
                              val cmd: String?,
                              val pompath: String?,
                              val poststepcmd: String?,
                              val rbgroup: String?,
                              val dockerimgname: String?,
                              val dockerimgtag: String?,
                              val dockerfilepath: String?,
                              val dockertoken: String?,
                              val dockerobjectcode: String?,
                              val scmbranch: String?) : Metadata