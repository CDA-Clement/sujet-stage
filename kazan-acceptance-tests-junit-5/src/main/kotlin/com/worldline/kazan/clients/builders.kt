package com.worldline.kazan.clients

import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.worldline.kazan.clients.ResourceManagementService.*
import com.worldline.kazan.logger
import feign.Feign
import feign.Logger
import feign.RequestTemplate
import feign.Response
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder
import java.util.*

object JacksonClientBuilder {
    val jsonDecoder = JacksonDecoder(listOf(KotlinModule()))
    val jsonEncoder = JacksonEncoder(listOf(KotlinModule()))

    inline fun <reified T : Any> createClient(url: String, noinline interceptor: (RequestTemplate) -> Unit = {}): T =
            Feign.builder()
                    .logger(Logger.JavaLogger())
                    .logLevel(Logger.Level.BASIC)
                    //.client(client)
                    .encoder(jsonEncoder)
                    .decoder(jsonDecoder)
                    .requestInterceptor(interceptor)
                    .target(T::class.java, url)

}

class ResourceManagement(val client: ResourceManagementService, @Suppress("CanBeParameter") val token: String = "") {

    val log by logger<ResourceManagement>()
    val base64encoder = Base64.getEncoder()!!
    val auth = "Bearer $token"

    fun isAdmin() = when (client.getAdminStatus(auth).status()) {
        204 -> true
        403 -> false
        else -> throw Exception("TODO - Bad response status")
    }

    fun getProjects() = client.getProjects(auth)

    fun project(project: String, init: Project.() -> Unit) =
            Project(project, "", "").run(init)

    fun Project.get(body: Response.() -> Unit) =
            client.getProject(auth, reference).run(body)

    fun Project.put(body: Response.() -> Unit) =
            client.createProject(auth, reference, this).run(body)

    fun Project.delete(body: Response.() -> Unit) =
            client.deleteProject(auth, reference).run(body)


    fun Project.getResources(type: String) =
            client.getProjectResourcesByType(auth, this.reference, type)

    fun Project.svn(ref: String, body: Resource.() -> Unit) =
            Resource("svnrepository", ref, this.reference).run(body)

    fun Project.hg(ref: String, body: Resource.() -> Unit) =
            Resource("hgrepository", ref, this.reference).run(body)

    fun Project.git(ref: String, body: Resource.() -> Unit) =
            Resource("gitrepository", ref, this.reference).run(body)

    fun Project.gitlab(ref: String, body: Resource.() -> Unit) =
            Resource("gitlab", ref, this.reference).run(body)

    fun Project.jenkins(ref: String, instance: Int = 1, body: Resource.() -> Unit) =
            Resource("jenkins-$instance", ref, this.reference).run(body)

    fun Project.reviewboard(ref: String, body: Resource.() -> Unit) =
            Resource("reviewboard", ref, this.reference).run(body)

    fun Resource.put(body: Response.() -> Unit) =
            client.putResource(auth, project, type, ref, this).run(body)

    fun Resource.get(body: Response.() -> Unit) =
            client.getResource(auth, project, type, ref).run(body)

    fun Resource.delete(body: Response.() -> Unit) =
            client.deleteResource(auth, project, type, ref).run(body)


    fun Project.permission(login: String, body: Permission.() -> Unit) =
            Permission(login, this.reference).run(body)

    fun Permission.put(body: Response.() -> Unit) =
            client.putPermission(auth, project, login, this).run(body)

    fun Permission.delete(body: Response.() -> Unit) =
            client.delPermission(auth, project, login).run(body)

}

class CredentialsBuilder(val resourceManagementService: ResourceManagementService) {

    val anonymousClient = ResourceManagement(resourceManagementService)

    val brokerAPIToken = "eyJhbGciOiJFUzI1NiIsImtpZCI6IktaTi1QUkQtMDAxIn0.eyJpc3MiOiJLYXphbiBVc2VyIE1hbmFnZW1lbnQiLCJhdWQiOiJLYXphbiBVc2VyIE1hbmFnZW1lbnQiLCJzdWIiOiJCcm9rZXIiLCJpZCI6MCwiaXNBZG1pbiI6dHJ1ZSwiZXhwIjozMjAxMTgxMzQ2LCJuYmYiOjE0NzMxODEyMjYsImp0aSI6IldfWnk4c2IycFRPU1o2NG9VbWRoUnciLCJpYXQiOjE0NzMxODEzNDZ9.zXgloc3RGy4A3AOKsI-O9h4mcIA4gfm97pUWsxTLO1Ymj5T6G0OvKQP_c9IJT2_0jqZyhUBTKQa1I6MTtKhCDA"

    val brokerClient = ResourceManagement(resourceManagementService, brokerAPIToken)

    fun anonymous(init: ResourceManagement.() -> Unit) = anonymousClient.init()

    fun brokerDo(init: ResourceManagement.() -> Unit) = brokerClient.init()

    fun client(init: ResourceManagementService.() -> Unit) = resourceManagementService.init()

    fun logUser(login: String, password: String = ""): (ResourceManagement.() -> Unit) -> Unit = { init ->
        ResourceManagement(resourceManagementService,
                           resourceManagementService.logMeIn(Credentials(login, password)).token).init()
    }

}

fun resourceManagement(init: CredentialsBuilder.() -> Unit) {
    val client = JacksonClientBuilder.createClient<ResourceManagementService>("http://localhost:12000")
    resourceManagement(client, init)
}

fun resourceManagement(client: ResourceManagementService, init: CredentialsBuilder.() -> Unit) {
    CredentialsBuilder(client).init()
}

