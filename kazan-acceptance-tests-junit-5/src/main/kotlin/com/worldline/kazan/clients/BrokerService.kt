package com.worldline.kazan.clients

import com.worldline.kazan.dtos.ExecutorTaskDTO
import feign.Headers
import feign.Param
import feign.RequestLine

interface BrokerService {
    @RequestLine("PUT /v1/executor")
    @Headers("Content-Type: application/json", "TagLog: {taglog}")
    fun putExecutorTaskDTO(@Param("taglog") taglog: String,
                           executorTaskDTO: ExecutorTaskDTO)
}