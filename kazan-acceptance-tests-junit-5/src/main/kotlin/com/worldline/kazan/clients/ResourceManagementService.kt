package com.worldline.kazan.clients

import com.fasterxml.jackson.annotation.JsonProperty
import com.worldline.kazan.dtos.KRole
import com.worldline.kazan.dtos.ProjectDTO
import com.worldline.kazan.dtos.ResourceDTO
import feign.Headers
import feign.Param
import feign.RequestLine
import feign.Response

interface ResourceManagementService {
    @RequestLine("GET /v1/users/adminStatus")
    @Headers("Authorization: {auth}")
    fun getAdminStatus(@Param("auth") auth: String): Response

    class Credentials(var login: String, var password: String)

    class Project(var reference: String, var name: String, var description: String, var metadata: String? = null)

    class Resource(@JsonProperty("type") val type: String,
                   @JsonProperty("reference") val ref: String,
                   @JsonProperty("project_ref") val project: String,
                   @JsonProperty("name") var name: String = "Name of $ref",
                   @JsonProperty("metadata") var metadata: String? = null)

    class Permission(@JsonProperty("user_id") val login: String,
                     @JsonProperty("project_ref") val project: String,
                     @JsonProperty("role") var role: String = KRole.UNKNOWN.toJsonValue!!)

    class Token {
        var token: String = ""

    }

    @RequestLine("POST /v1/users/logmein")
    @Headers("Content-Type: application/json")
    fun logMeIn(credentials: Credentials): Token

    @RequestLine("GET /v1/projects/")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProjects(@Param("auth") auth: String): List<ProjectDTO>

    @RequestLine("GET /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProject(@Param("auth") auth: String,
                   @Param("project") reference: String): Response

    @RequestLine("GET /v1/projects/{project}/resources/types/{type}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProjectResourcesByType(@Param("auth") auth: String,
                                  @Param("project") reference: String,
                                  @Param("type") type: String): List<ResourceDTO>

    @RequestLine("PUT /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun createProject(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      project: Project): Response

    @RequestLine("DELETE /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun deleteProject(@Param("auth") auth: String,
                      @Param("project") reference: String): Response


    @RequestLine("GET /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String): Response

    @RequestLine("PUT /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun putPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String,
                      permission: Permission): Response

    @RequestLine("DELETE /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun delPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String): Response

    @RequestLine("GET /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getResource(@Param("auth") auth: String,
                    @Param("project") project: String,
                    @Param("type") type: String,
                    @Param("ref") ref: String): Response


    @RequestLine("PUT /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun putResource(@Param("auth") auth: String,
                    @Param("project") project: String,
                    @Param("type") type: String,
                    @Param("ref") ref: String,
                    resource: Resource): Response

    @RequestLine("DELETE /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun deleteResource(@Param("auth") auth: String,
                       @Param("project") project: String,
                       @Param("type") type: String,
                       @Param("ref") ref: String): Response
}