package com.worldline.kazan.clients

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.google.gson.FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES
import com.google.gson.GsonBuilder
import feign.Response

fun Response.is2xx(): Boolean = status() in 200..299
fun Response.is3xx(): Boolean = status() in 300..399
fun Response.is4xx(): Boolean = status() in 400..499
fun Response.is5xx(): Boolean = status() in 500..599

val gson = GsonBuilder().setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create()!!
val jackson = jacksonObjectMapper()

inline fun <reified T : Any> Response.readBody(): T =
        if (String::class.java.isAssignableFrom(T::class.java)) body().asReader().readText() as T
        else jackson.readValue(body().asReader(), T::class.java)
