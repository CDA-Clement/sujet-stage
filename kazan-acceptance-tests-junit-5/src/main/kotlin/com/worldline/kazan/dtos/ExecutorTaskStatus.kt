package com.worldline.kazan.dtos

enum class ExecutorTaskStatus(val isTerminal: Terminal? = null, val toJsonValue: String? = null) {

    CREATED(Terminal.NO, "created"),  //
    PROCESSING(Terminal.NO, "processing"),  //
    OK(Terminal.YES, "ok"),  //
    KO(Terminal.YES, "ko"),  //
    UNKNOWN(Terminal.UNKNOWN, "unknown");

    enum class Terminal {
        YES, NO, UNKNOWN
    }

    companion object {
        /**
         * Try to find an enum value with the same given value (ignore case). If not found, return UNKNOWN.
         *
         * @param jsonValue the value to parse
         *
         * @return the enum corresponding to the jsonValue
         */
        fun fromJSon(jsonValue: String?): ExecutorTaskStatus {
            for (type in values()) {
                if (type.toJsonValue.equals(jsonValue, ignoreCase = true)) {
                    return type
                }
            }
            return UNKNOWN
        }
    }
}
