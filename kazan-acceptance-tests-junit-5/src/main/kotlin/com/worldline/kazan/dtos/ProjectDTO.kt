package com.worldline.kazan.dtos

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*


class ProjectDTO(
        @JsonProperty("reference")
        val reference: String? = null,
        @JsonProperty("name")
        val name: String? = null,
        @JsonProperty("description")
        val description: String? = null,
        @JsonProperty("client")
        val client: String? = null,
        @JsonProperty("creation_date")
        val creationDate: Date? = null,
        @JsonProperty("sync_state")
        val syncState: SynchronizationState? = null,
        @JsonProperty("metadata")
        val metadata: String? = null
)
