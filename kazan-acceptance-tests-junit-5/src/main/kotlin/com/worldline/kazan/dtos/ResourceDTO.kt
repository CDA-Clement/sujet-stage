package com.worldline.kazan.dtos

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class ResourceDTO(
        @JsonProperty("type")
        val type: String? = null,
        @JsonProperty("reference")
        val reference: String? = null,
        @JsonProperty("project_ref")
        val projectRef: String? = null,
        @JsonProperty("executor_ref")
        val executorRef: String? = null,
        @JsonProperty("name")
        val name: String? = null,
        @JsonProperty("creation_date")
        val creationDate: Date? = null,
        @JsonProperty("pending_sync_state")
        val pendingSyncState: SynchronizationState? = null,
        @JsonProperty("sync_state")
        val syncState: SynchronizationState? = null,
        @JsonProperty("metadata")
        val metadata: String? = null
)
