package com.worldline.kazan.dtos

enum class SynchronizationState(val toJsonValue: String? = null, val ongoing: Boolean = false) {

    ALIVE("alive", false),

    BROKEN("broken", false),

    ISAAC("isaac", true),

    ZOMBIE("zombie", true),

    GHOST("ghost", false),

    UNKNOWN("unknown", true);

    companion object {
        /**
         * Try to find an enum value with the same given value (ignore case). If not found, return UNKNOWN.
         *
         * @param jsonValue the value to parse
         *
         * @return the enum corresponding to the jsonValue
         */
        fun fromJSon(jsonValue: String?): SynchronizationState {
            for (type in SynchronizationState.values()) {
                if (type.toJsonValue.equals(jsonValue, ignoreCase = true)) {
                    return type
                }
            }
            return UNKNOWN
        }
    }
}
