package com.worldline.kazan.dtos

import com.fasterxml.jackson.annotation.JsonProperty


class ExecutorTaskDTO(
        @JsonProperty("task_id")
        val taskId: Long? = null,
        @JsonProperty("project_ref")
        val projectRef: String? = null,
        @JsonProperty("resource_ref")
        val resourceRef: String? = null,
        @JsonProperty("resource_type")
        val resourceType: String? = null,
        @JsonProperty("status")
        val status: ExecutorTaskStatus? = null,
        @JsonProperty("task_type")
        val taskType: ExecutorTaskType? = null,
        @JsonProperty("priority")
        val priority: Int? = null,
        @JsonProperty("taglog")
        val taglog: String? = null,
        @JsonProperty("metadata")
        val metadata: String? = null
)
