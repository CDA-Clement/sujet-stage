package com.worldline.kazan.dtos

enum class ExecutorTaskType(val action: Action? = null, val family: Family? = null, val toJsonValue: String? = null) {

    ADD_PERMISSION(Action.ADD, Family.PERMISSION, "add_permission"),  //
    DEL_PERMISSION(Action.REMOVE, Family.PERMISSION, "del_permission"),  //
    ADD_TECHNICAL_USER(Action.ADD, Family.TECHNICAL_USER, "add_technical_user"),  //
    DEL_TECHNICAL_USER(Action.REMOVE, Family.TECHNICAL_USER, "del_technical_user"),  //
    ADD_RESOURCE(Action.ADD, Family.RESOURCE, "add_resource"),  //
    DEL_RESOURCE(Action.REMOVE, Family.RESOURCE, "del_resource"),  //
    AUDIT_RESOURCE(Action.AUDIT, Family.AUDIT, "audit_resource"),  //
    UNKNOWN(Action.UNKNOWN, Family.UNKNOWN, "unknown");

    enum class Family {
        PERMISSION, RESOURCE, AUDIT, TECHNICAL_USER, UNKNOWN
    }

    enum class Action {
        ADD, REMOVE, AUDIT, UNKNOWN
    }

    companion object {
        /**
         * Try to find an enum value with the same given value (ignore case). If not found, return UNKNOWN.
         *
         * @param jsonValue the value to parse
         *
         * @return the enum corresponding to the jsonValue
         */
        fun fromJSon(jsonValue: String?): ExecutorTaskType {
            for (type in values()) {
                if (type.toJsonValue.equals(jsonValue, ignoreCase = true)) {
                    return type
                }
            }
            return UNKNOWN
        }
    }
}
