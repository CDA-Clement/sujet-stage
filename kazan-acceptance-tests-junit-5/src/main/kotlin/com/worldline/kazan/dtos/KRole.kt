package com.worldline.kazan.dtos

import com.worldline.kazan.dtos.KAction.*

enum class KRole(val toJsonValue: String?, vararg allowedActions: KAction?) {
    ADMIN("admin",
          UPDATE_PROJECT,
          READ_PROJECT,
          DELETE_PROJECT,
          CREATE_RESOURCE,
          UPDATE_RESOURCE,
          READ_RESOURCE,
          DELETE_RESOURCE),
    DEVELOPER("developer",
              READ_PROJECT,
              CREATE_RESOURCE,
              UPDATE_RESOURCE,
              READ_RESOURCE,
              DELETE_RESOURCE),
    VIEWER("viewer",
           READ_PROJECT,
           READ_RESOURCE),
    EXTERNAL("external"),
    UNKNOWN("unknown");

    private val allowedActions: List<KAction?>

    /**
     * @param action action to check if it is allowed
     *
     * @return true iff the [KAction] can do this action
     */
    fun isAllowedTo(action: KAction?): Boolean {
        return allowedActions.contains(action)
    }

    /**
     * @param role reference role
     *
     * @return true iff the [KRole] is at the same level or at a higher level
     */
    fun isAtLeast(role: KRole?): Boolean {
        return if (role == null) { // is at least "null" is like is at least "nothing"
            // so return true
            true
        } else when (role) {
            EXTERNAL ->  // here we can do:
                // return this == EXTERNAL || isAtLeast(VIEWER);
                // but external is the lowest level so it could be
                // simplified into:
                true
            VIEWER -> this == VIEWER || isAtLeast(DEVELOPER)
            DEVELOPER -> this == DEVELOPER || isAtLeast(ADMIN)
            ADMIN -> this == ADMIN
            else -> throw IllegalArgumentException("Unknown role: $role")
        }
    }

    companion object {
        /**
         * Try to find an enum value with the same given value (ignore case). If not found, return UNKNOWN.
         *
         * @param jsonValue the value to parse
         *
         * @return the enum corresponding to the jsonValue
         */
        fun fromJSon(jsonValue: String?): KRole? {
            for (type in values()) {
                if (type.toJsonValue.equals(jsonValue, ignoreCase = true)) {
                    return type
                }
            }
            return UNKNOWN
        }
    }

    init {
        this.allowedActions = allowedActions.toList()
    }
}
