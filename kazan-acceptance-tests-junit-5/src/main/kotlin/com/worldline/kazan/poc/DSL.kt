package com.worldline.kazan.poc

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.worldline.kazan.dtos.KRole
import com.worldline.kazan.dtos.ProjectDTO
import com.worldline.kazan.dtos.ResourceDTO
import com.worldline.kazan.logger
import feign.*
import feign.jackson.JacksonDecoder
import feign.jackson.JacksonEncoder


// REST Client Builder

object JacksonClientBuilder {
    val jsonDecoder = JacksonDecoder(listOf(KotlinModule()))
    val jsonEncoder = JacksonEncoder(listOf(KotlinModule()))
    inline fun <reified T : Any> createClient(url: String, noinline interceptor: (RequestTemplate) -> Unit = {}): T =
            Feign.builder()
                    .logger(Logger.JavaLogger())
                    .logLevel(Logger.Level.BASIC)
                    //.client(client)
                    .encoder(jsonEncoder)
                    .decoder(jsonDecoder)
                    .requestInterceptor(interceptor)
                    .target(T::class.java, url)
}


// APIS

interface ResourcesAPI {

    class Credentials(var login: String, var password: String, val sudo: Boolean = true)
    class Favourite(val projectReference: String)

    class Project(var reference: String, var name: String, var description: String, var metadata: String? = null)
    class Resource(@JsonProperty("type") val type: String,
                   @JsonProperty("reference") val ref: String,
                   @JsonProperty("project_ref") val project: String,
                   @JsonProperty("name") var name: String = "Name of $ref",
                   @JsonProperty("metadata") var metadata: String? = null)

    class Permission(@JsonProperty("user_id") val login: String,
                     @JsonProperty("project_ref") val project: String,
                     @JsonProperty("role") var role: String = KRole.UNKNOWN.toJsonValue!!)

    class Token(var token: String = "")


    @RequestLine("POST /v1/users/logmein")
    @Headers("Content-Type: application/json")
    fun logMeIn(credentials: Credentials): Token

    @RequestLine("GET /v1/users/{login}/favourites")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getFavourites(@Param("login") login: String, @Param("auth") auth: String): List<Favourite>

    @RequestLine("GET /v1/users/adminStatus")
    @Headers("Authorization: {auth}")
    fun getAdminStatus(@Param("auth") auth: String): Response

    @RequestLine("GET /v1/projects/")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProjects(@Param("auth") auth: String): List<ProjectDTO>

    @RequestLine("GET /v1/users/{login}/projects")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getMyProjects(@Param("login") login: String, @Param("auth") auth: String): List<ProjectDTO>

    @RequestLine("GET /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProject(@Param("auth") auth: String,
                   @Param("project") reference: String): Response

    @RequestLine("GET /v1/projects/{project}/resources/types/{type}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getProjectResourcesByType(@Param("auth") auth: String,
                                  @Param("project") reference: String,
                                  @Param("type") type: String): List<ResourceDTO>

    @RequestLine("PUT /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun createProject(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      project: Project): Response

    @RequestLine("DELETE /v1/projects/{project}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun deleteProject(@Param("auth") auth: String,
                      @Param("project") reference: String): Response


    @RequestLine("GET /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String): Response

    @RequestLine("PUT /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun putPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String,
                      permission: Permission): Response

    @RequestLine("DELETE /v1/projects/{project}/permissions/logins/{login}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun delPermission(@Param("auth") auth: String,
                      @Param("project") reference: String,
                      @Param("login") login: String): Response

    @RequestLine("GET /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun getResource(@Param("auth") auth: String,
                    @Param("project") project: String,
                    @Param("type") type: String,
                    @Param("ref") ref: String): Response


    @RequestLine("PUT /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun putResource(@Param("auth") auth: String,
                    @Param("project") project: String,
                    @Param("type") type: String,
                    @Param("ref") ref: String,
                    resource: Resource): Response

    @RequestLine("DELETE /v1/projects/{project}/resources/types/{type}/references/{ref}")
    @Headers("Content-Type: application/json", "Authorization: {auth}")
    fun deleteResource(@Param("auth") auth: String,
                       @Param("project") project: String,
                       @Param("type") type: String,
                       @Param("ref") ref: String): Response
}


// DSL Creation


class ResourcesBuilder(private val resourcesClient: ResourcesAPI, val login: String = "", val token: String = "") {
    val log by logger<ResourcesBuilder>()
    val auth = "Bearer $token"

    fun isAdmin() = when (resourcesClient.getAdminStatus(auth).status()) {
        204 -> true
        403 -> false
        else -> throw Exception("TODO - Bad response status")
    }

    fun getProjects() = resourcesClient.getProjects(auth)
    fun getMyProjects(login: String) = resourcesClient.getMyProjects(login, auth)

    fun getMyFavourites() = resourcesClient.getFavourites(login, auth)

    fun project(project: String, init: ResourcesAPI.Project.() -> Unit) =
            ResourcesAPI.Project(project, "", "").run(init)

    fun ResourcesAPI.Project.get(body: Response.() -> Unit) =
            resourcesClient.getProject(auth, reference).run(body)

    fun ResourcesAPI.Project.put(body: Response.() -> Unit) =
            resourcesClient.createProject(auth, reference, this).run(body)

    fun ResourcesAPI.Project.delete(body: Response.() -> Unit) =
            resourcesClient.deleteProject(auth, reference).run(body)


    fun ResourcesAPI.Project.getResources(type: String) =
            resourcesClient.getProjectResourcesByType(auth, this.reference, type)


    fun ResourcesAPI.Project.gitlab(ref: String, body: ResourcesAPI.Resource.() -> Unit) =
            ResourcesAPI.Resource("gitlab", ref, this.reference).run(body)

    fun ResourcesAPI.Resource.put(body: Response.() -> Unit) =
            resourcesClient.putResource(auth, project, type, ref, this).run(body)

    fun ResourcesAPI.Resource.get(body: Response.() -> Unit) =
            resourcesClient.getResource(auth, project, type, ref).run(body)

    fun ResourcesAPI.Resource.delete(body: Response.() -> Unit) =
            resourcesClient.deleteResource(auth, project, type, ref).run(body)


    fun ResourcesAPI.Project.permission(login: String, body: ResourcesAPI.Permission.() -> Unit) =
            ResourcesAPI.Permission(login, this.reference).run(body)

    fun ResourcesAPI.Permission.put(body: Response.() -> Unit) =
            resourcesClient.putPermission(auth, project, login, this).run(body)

    fun ResourcesAPI.Permission.delete(body: Response.() -> Unit) =
            resourcesClient.delPermission(auth, project, login).run(body)

}


class AuthenticationBuilder(private val authenticationAPI: ResourcesAPI) {
    private val anonymousClient = ResourcesBuilder(authenticationAPI)

    fun anonymous(init: ResourcesBuilder.() -> Unit) = anonymousClient.init()

    fun authenticate(login: String, password: String = ""): (ResourcesBuilder.() -> Unit) -> Unit =
            { init ->
                val token = authenticationAPI.logMeIn(ResourcesAPI.Credentials(login, password)).token
                ResourcesBuilder(authenticationAPI, login, token).init()
            }
}

fun kazan(client: ResourcesAPI = JacksonClientBuilder.createClient("https://kazan.atosworldline.com/api"),
          init: AuthenticationBuilder.() -> Unit) {
    AuthenticationBuilder(client).init()
}


// test

fun main(args: Array<String>) {

    kazan {

        val antoine = authenticate("a183276", password)

        anonymous {
            println("Hello from anonymous!")
        }

        antoine {
            println("Hello from $login!")

            println("---------")

            println("My favourites are:")

            getMyFavourites().forEach { println("- ${it.projectReference}") }

            println("Creating a project")
            project("coucou-clement") {
                description = "test from dsl"
                name = "Test Clement"
                gitlab("coucou") {

                }
                delete { println("${status()}") }
            }
        }

    }

}