package com.worldline.kazan

import org.slf4j.LoggerFactory

inline fun <reified T : Any> logger() = lazy { LoggerFactory.getLogger(T::class.java)!! }
fun logger(name: String) = lazy { LoggerFactory.getLogger(name)!! }

