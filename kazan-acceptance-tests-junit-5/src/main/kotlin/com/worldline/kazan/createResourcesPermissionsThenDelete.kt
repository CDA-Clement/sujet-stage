package com.worldline.kazan

import com.worldline.kazan.clients.*
import com.worldline.kazan.dtos.KRole
import java.time.Duration
import java.util.concurrent.Callable
import java.util.concurrent.Executors
import java.util.concurrent.Future

class CreateResourceScenario(val user: String = "a100000", val project: String = "a100000_project1") {

    val log by logger<CreateResourceScenario>()

    val threadpool = Executors.newCachedThreadPool()!!

    val brokerClient = JacksonClientBuilder.createClient<BrokerService>("http://localhost:12002")

    val Int.ms: Duration get() = Duration.ofMillis(toLong())
    val Int.s: Duration get() = Duration.ofSeconds(toLong())
    fun <T> after(delay: Duration, execute: () -> T): Future<T> =
            threadpool.submit(Callable {
                Thread.sleep(delay.toMillis())
                execute()
            })

    fun test() {
        resourceManagement {
            val a100000 = logUser(user)
            a100000 {
                project(project) {
                    name = "Project of user $user"
                    description = "My project is cool"

                    put {
                        if (is2xx()) {
                            log.info("project creation is OK")
                        } else {
                            log.info("oups ${status()}")
                        }
                    }

                    log.info("Press enter to continue")
                    System.`in`.read()

                    svn("mon-svn") {
                        name = "Mon SVN"
                        put {
                            if (is2xx()) {
                                log.info("svn ok!!")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }

                    jenkins("my-job") {
                        name = "My jenkins job"
                        metadata = JobTypeInformation("FREESTYLE", "my jenkins job",
                                                      null,
                                                      "GIT", "http://kazan.priv.atos.fr/git/mygitrepo",
                                                      null,
                                                      "pwd",
                                                      null, null, null, null, null, null, null, null, null)
                                .toMetadata()

                        put {
                            if (is2xx()) {
                                log.info("jenkins job ok!!")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }

                    git("mon-git") {
                        put {
                            if (is2xx()) {
                                log.info("git ok!!")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }

                    //                        scenario(("Check that 'mon-svn' exists")) {
                    //                            svn("mon-svn") {
                    //                                get {
                    //                                    if (is2xx()) {
                    //                                        val body: Map<String, Any> = readBody()
                    //                                        log.info(body.toString())
                    //                                    } else {
                    //                                        log.info("oups ${status()}")
                    //                                    }
                    //                                }
                    //                            }
                    //                        }

                    log.info("Press enter to continue")
                    System.`in`.read()

                    permission("a149812") {
                        role = KRole.VIEWER.toJsonValue!!
                        put {
                            if (is2xx()) {
                                log.info("Successfuly add my friend :)")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }
                    permission("a149813") {
                        role = KRole.DEVELOPER.toJsonValue!!
                        put {
                            if (is2xx()) {
                                log.info("Successfuly add my friend :)")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }
                    log.info("Press enter to continue")
                    System.`in`.read()
                    permission("a149812") {
                        delete {
                            if (is2xx()) {
                                log.info("Done")
                            } else {
                                log.info("oups ${status()}")
                            }
                        }
                    }
                }
            }
        }
    }

    fun cleanup() {
        com.worldline.kazan.poc.kazan {
            (authenticate(user)) {
                project(project) {
                    delete {
                        if (is2xx()) {
                            log.info("project deletion is OK")
                        } else {
                            log.info("oups ${status()}")
                        }

                    }
                }
            }
        }
    }


    fun stop() {
        log.info("Stop threadpool")
        threadpool.shutdown()
        log.info("Done")
    }

}


fun main(args: Array<String>) {

    with(CreateResourceScenario()) {
        test()
        log.info("Press enter to delete the project")
        System.`in`.read()
        cleanup()
        log.info("Press enter to exit")
        System.`in`.read()
        stop()
    }

}